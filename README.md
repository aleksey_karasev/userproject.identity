Добрый день!
Это проект аутентификации и авторизации для проекта UserProject.
Проект выполнен в простом стиле, с локальным View, для осуществления безопасности при передачи данных при регистрации. В случае необходимости в преокт можно добавить документация для обращения API.
В проекте 2 endpoints:
/identity/Login - для входа в систему, в случае успхеха получаете токен.
/identity/Register - для регистрации, в случае успеха добавляется запись в БД и получаете токен.

Токен служит для работы с проектом UserProject, в нем есть поле для ввода в Swagger, под названием Authentication.

Либо используя какой-либо клиент, можно добавлять данный токен автоматически в Header браузера, для запросов.

----------------------------------------------

Good afternoon
This is an authentication and authorization project for the UserProject.
The project is made in a simple style, with a local View, to ensure security during data transfer during registration. If necessary, you can add documentation for calling the API to the project.
The project has 2 endpoints:
/identity/Login - to log in, if successful, you will receive a token.
/identity/Register - for registration, if successful, an entry is added to the database and you receive a token.

The token is used to work with the UserProject, it has an input field in Swagger called Authentication.

Or using any client, you can add this token automatically to the browser Header for requests.
