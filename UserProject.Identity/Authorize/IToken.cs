﻿using Microsoft.AspNetCore.Identity;

namespace UserProject.Identity.Authorize
{
    public interface IToken
    {
        public string GenerateToken(IdentityUser user, string role);
    }
}
