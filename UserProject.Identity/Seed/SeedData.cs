﻿using Microsoft.AspNetCore.Identity;

namespace UserProject.Identity.Seed
{
    public static class SeedData
    {
        public static async Task Initialize(IServiceProvider serviceProvider)
        {
            using (var serviceScope = serviceProvider.CreateScope())
            {
                var roleManager = serviceScope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();

                if (!roleManager.Roles.Any())
                {
                    string[] roleNames = { "Employeer", "Manager", "Supervisor" };

                    foreach (var roleName in roleNames)
                    {
                        await roleManager.CreateAsync(new IdentityRole(roleName));
                    }
                }

                var userManager = serviceScope.ServiceProvider.GetRequiredService<UserManager<IdentityUser>>();

                if (!userManager.Users.Any())
                {
                    string password = "123qwe!@#QWE";

                    IdentityUser[] users = { 
                        new(){
                            Email = "Admin@gmail.com",
                            UserName = "Admin@gmail.com"
                        },
                        new(){
                            Email = "Employeer@gmail.com",
                            UserName = "Employeer@gmail.com"
                        },
                        new(){
                            Email = "Manager@gmail.com",
                            UserName = "Manager@gmail.com"
                        },
                    };

                    foreach (var user in users)
                    {
                        await userManager.CreateAsync(user, password);

                        if(user.Email == "Admin@gmail.com") { await userManager.AddToRoleAsync(user, "Supervisor"); }
                        if(user.Email == "Employeer@gmail.com") { await userManager.AddToRoleAsync(user, "Employeer"); }
                        if(user.Email == "Manager@gmail.com") { await userManager.AddToRoleAsync(user, "Manager"); }
                    }

                }
            }
        }
    }
}
