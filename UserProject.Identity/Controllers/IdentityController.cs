﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

using UserProject.Identity.Models;
using UserProject.Identity.Authorize;


namespace UserProject.Identity.Controllers
{
    public class IdentityController : Controller
    {
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly UserManager<IdentityUser> _userManager;
        private readonly IToken _token;

        public IdentityController(
            SignInManager<IdentityUser> signInManager,
            UserManager<IdentityUser> userManager,
            IToken token)
        {
            _signInManager = signInManager;
            _userManager = userManager;

            _token = token;
        }

        [HttpGet]
        public IActionResult Login()
        {
            var viewModel = new LoginViewModel();

            return View("Login", viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Login(LoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var user = await _userManager.FindByEmailAsync(viewModel.Email);

                if (user == null)
                {
                    ModelState.AddModelError(string.Empty, "Login error");
                }
                else
                {
                    var resAuth = await _signInManager.PasswordSignInAsync(viewModel.Email,
                        viewModel.Password, false, false);

                    if (resAuth.Succeeded)
                    {
                        var role = await _userManager.GetRolesAsync(user);
                        var token = _token.GenerateToken(user, role?.FirstOrDefault());

                        return Json(new { Token = token });
                    }
                    else { ModelState.AddModelError(string.Empty, "Login error"); }
                }
            }

            return BadRequest(ModelState);
        }

        [HttpGet]
        public IActionResult Register()
        {
            var viewModel = new RegisterViewModel();

            return View("Register", viewModel);
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterViewModel viewModel)
        {

            if (!ModelState.IsValid || !viewModel.Password.Equals(viewModel.ConfirmPassword))
            {
                ModelState.AddModelError(string.Empty, "Register error");
            }
            else
            {
                var user = new IdentityUser()
                {
                    Email = viewModel.Email,
                    UserName = viewModel.Email,
                };

                var result = await _userManager.CreateAsync(user, viewModel.Password);

                if (result.Succeeded)
                {
                    await _userManager.AddToRoleAsync(user, viewModel.IsManager ? "Manager" : "Employeer");

                    await _signInManager.SignInAsync(user, false);

                    var role = await _userManager.GetRolesAsync(user);
                    var token = _token.GenerateToken(user, role.FirstOrDefault());

                    return Json(new { Token = token });
                }
                else { ModelState.AddModelError(string.Empty, "Register error"); }
            }

            return BadRequest(ModelState);
        }
    }
}
