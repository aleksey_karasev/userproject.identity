using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using UserProject.Identity.Authorize;
using UserProject.Identity.Data;
using UserProject.Identity.Seed;

var builder = WebApplication.CreateBuilder(args);

var connectionString = builder.Configuration.GetConnectionString("IdentityDbContextConnection") ?? throw new InvalidOperationException("Connection string 'IdentityDbContextConnection' not found.");

builder.Services.AddDbContext<IdentityProjectContext>(options => options.UseSqlServer(connectionString));

builder.Services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<IdentityProjectContext>();

builder.Services.AddScoped<IToken, Token>();

builder.Services.AddControllersWithViews();

var app = builder.Build();

await SeedData.Initialize(app.Services);

app.UseCors(options =>
{
    options
        .AllowAnyOrigin()
        .AllowAnyHeader()
        .AllowAnyMethod();
});

app.UseStaticFiles();

app.UseAuthentication();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.UseEndpoints(endpoints =>
{
    endpoints.MapDefaultControllerRoute();
});

app.Use(async (context, next) =>
{
    if (context.Request.Path == "/")
    {
        context.Response.Redirect("/identity/login");
        return;
    }

    await next();
});

app.Run();
