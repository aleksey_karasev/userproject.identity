﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace UserProject.Identity.Data;

public class IdentityProjectContext : IdentityDbContext<IdentityUser>
{
    public IdentityProjectContext(DbContextOptions<IdentityProjectContext> options)
        : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
        base.OnModelCreating(builder);

        builder.Entity<IdentityUser>(user => user.ToTable(name: "Users"));
        builder.Entity<IdentityRole>(user => user.ToTable(name: "Roles"));
        builder.Entity<IdentityUserRole<string>>(user => user.ToTable(name: "UserRoles"));
        builder.Entity<IdentityUserClaim<string>>(user => user.ToTable(name: "UserClaims"));
        builder.Entity<IdentityUserLogin<string>>(user => user.ToTable(name: "UserLogins"));
        builder.Entity<IdentityUserToken<string>>(user => user.ToTable(name: "UserToken"));
        builder.Entity<IdentityRoleClaim<string>>(user => user.ToTable(name: "RoleClaims"));
      
    }
}
